import {
    FETCHING_USER_ILLUSTS, FETCHING_USER_ILLUSTS_SUCCESS, FETCHING_USER_ILLUSTS_FAILURE,
    FETCHING_USER_DETAIL, FETCHING_USER_DETAIL_SUCCESS, FETCHING_USER_DETAIL_FAILURE,
    FETCHING_USER_MANGA, FETCHING_USER_MANGA_SUCCESS, FETCHING_USER_MANGA_FAILURE
}
    from '../actions/constants';
const initialState = {
    data: [],
    dataManga: [],
    detail: {},
    userIllustStatus: {
        isFetching: false,
        dataFetched: false,
        isFetchingMore: false,
        dataMoreFetched: false,
        error: false
    },
    userMangaStatus: {
        isFetching: false,
        dataFetched: false,
        isFetchingMore: false,
        dataMoreFetched: false,
        error: false
    },
    userDetailStatus: {
        isFetching: false,
        dataFetched: false,
        error: false
    },
    nextURL: '',
};

export default function userIllustReducer(state = initialState, action) {
    switch (action.type) {
        case FETCHING_USER_ILLUSTS:
            return {
                ...state,
                userIllustStatus: {
                    ...state.userIllustStatus,
                    isFetching: true
                }
            };
        case FETCHING_USER_ILLUSTS_SUCCESS:
            return {
                ...state,
                userIllustStatus: {
                    ...state.userIllustStatus,
                    isFetching: false,
                    dataFetched: true
                },
                data: action.data.data.illusts,
                nextURL: action.data.data.next_url
            };
        case FETCHING_USER_ILLUSTS_FAILURE:
            return {
                ...state,
                userIllustStatus: {
                    ...state.userIllustStatus,
                    isFetching: false,
                    error: true
                }
            };
        case FETCHING_USER_MANGA:
            return {
                ...state,
                userMangaStatus: {
                    ...state.userMangaStatus,
                    isFetching: true
                }
            };
        case FETCHING_USER_MANGA_SUCCESS:
            return {
                ...state,
                userMangaStatus: {
                    ...state.userMangaStatus,
                    isFetching: false,
                    dataFetched: true
                },
                dataManga: action.data.data.illusts,
                nextURL: action.data.data.next_url
            };
        case FETCHING_USER_MANGA_FAILURE:
            return {
                ...state,
                userMangaStatus: {
                    ...state.userMangaStatus,
                    isFetching: false,
                    error: true
                }
            };
        case FETCHING_USER_DETAIL:
            return {
                ...state,
                userDetailStatus: {
                    ...state.userDetailStatus,
                    isFetching: true
                }
            };
        case FETCHING_USER_DETAIL_SUCCESS:
            return {
                ...state,
                userDetailStatus: {
                    ...state.userDetailStatus,
                    isFetching: false,
                    dataFetched: true
                },
                detail: action.detail
            };
        case FETCHING_USER_DETAIL_FAILURE:
            return {
                ...state,
                userDetailStatus: {
                    ...state.userDetailStatus,
                    isFetching: false,
                    error: true
                }
            };
        default:
            return state;
    }
}