// Set up your application entry point here...
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
//Include Metronic Theme styles
import './libs/metronic_theme/plugins/bootstrap/css/bootstrap.min.css';
import './libs/metronic_theme/general/components-md.css';
import './libs/metronic_theme/general/plugins-md.css';
import './libs/metronic_theme/general/layout.css';
import './libs/metronic_theme/general/light.css';
//Include Site styles
import './styles/styles.scss';
import Feed from './components/Feed.jsx';
import Ranking from './components/Ranking.jsx';
import RankingDetail from './components/RankingDetail.jsx';
import Gif from './components/Gif.jsx';
import GifDetail from './components/GifDetail.jsx';
import UserProfile from './components/UserProfile.jsx';
import SideBar from './components/SideBar/SideBar';
//Applying Redux
import storeConfig from './storeConfig';
import { Provider } from 'react-redux';

const store = storeConfig();


const PrimaryLayout = () => (
  <div>
    <SideBar/>
    <div className="page-content-wrapper">
      <div className="page-content">
        <Switch>
          <Route exact path="/" component={Ranking} />
          <Route exact path="/feed" component={Feed} />
          <Route exact path="/ranking" component={Ranking} />
          <Route exact path="/ranking/detail/:id" component={RankingDetail}/>
          <Route exact path="/gif" component={Gif} />
          <Route exact path="/gif/detail/:id" component={GifDetail}/>
          <Route path="/user/profile/:id" component={UserProfile}/>
        </Switch>
      </div>
    </div>
  </div>
);

ReactDOM.render(
    <Provider store={store}>
      <Router>
        <PrimaryLayout/>
      </Router>
    </Provider>,
    document.getElementById('app')
);
