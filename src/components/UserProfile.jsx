import React from 'react';
import { connect } from 'react-redux';
import { fetchingUserDetail, fetchUserWorks } from '../actions/user_work_fetch';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Grid from './Grid/Grid.jsx';
import StringHelper from 'underscore.string';
import { Link } from 'react-router-dom';

class Profile extends React.Component {
    constructor() {
        super();
    }
    componentDidMount() {
        this.props.fetchingUserDetail(this.props.match.params.id);
        this.props.fetchUserWorks('illust', this.props.match.params.id);
        this.props.fetchUserWorks('manga', this.props.match.params.id);
    }
    componentWillReceiveProps(newProps) {
        if (newProps.match.params != this.props.match.params) {
            this.props.fetchingUserDetail(newProps.match.params.id);
            this.props.fetchUserWorks('illust', newProps.match.params.id);
            this.props.fetchUserWorks('manga', this.props.match.params.id);
        }
    }
    renderUserProfile = () => {
        if (this.props.userData.userDetailStatus.dataFetched) {
            let user = this.props.userData.detail.user;
            let profile = this.props.userData.detail.profile;
            return (
                <div className="profile-sidebar">
                    <div className="portlet light profile-sidebar-portlet ">
                        <div className="profile-usertitle">
                            <div className="profile-usertitle-name">
                                <p>{user.name}</p>
                                {!StringHelper.isBlank(profile.gender) ? <p className="gender text-left"><i className="fa fa-user-circle"></i> Gender: {StringHelper.capitalize(profile.gender)}</p> : ''}
                                {!StringHelper.isBlank(profile.twitter_url) ? <p className="twitter text-left"><i className="fa fa-twitter"></i> Twitter: <a target="_blank" href={profile.twitter_url}>{profile.twitter_url}</a></p> : ''}
                                {!StringHelper.isBlank(profile.webpage) ? <p className="webpage text-left"><i className="fa fa-globe"></i> Web Page: <a target="_blank" href={profile.webpage}>{profile.webpage}</a></p> : ''}
                            </div>
                        </div>
                    </div>
                    <div className="portlet light ">
                        <div className="row list-separated profile-stat">
                            <div className="col-md-6">
                                <div className="uppercase profile-stat-title"> {profile.total_illusts} </div>
                                <div className="uppercase profile-stat-text"> Illustrations </div>
                            </div>
                            <div className="col-md-6">
                                <div className="uppercase profile-stat-title"> {profile.total_manga} </div>
                                <div className="uppercase profile-stat-text"> Manga </div>
                            </div>
                        </div>
                        <div>
                            <h4 className="profile-desc-title">About {user.name}</h4>
                            <p className="profile-desc-text">{user.comment}</p>
                        </div>
                    </div>
                </div>
            );
        } else {
            return;
        }

    }
    initUserIllustGrid = () => {
        let gridData = {
            lazyloading: true,
            data: this.props.userData.userIllustStatus.dataFetched ? this.props.userData.data : [],
            columns: {
                columnsCount: 3,
                responsive: {
                    lg: 3,
                    md: 3,
                    sm: 3,
                    xs: 1,
                },
                content: {
                    format: (item) => {
                        return (
                            <div className="portlet light hvr-float">
                                <div className="portlet-body">
                                    <div className="illust-container">
                                        <Link to={`/ranking/detail/${item.id}`}>
                                            <img className="img-responsive" src={item.image_urls.px_480mw} />
                                            {
                                                item.meta_pages.length > 0 ?
                                                    <div className="illust-count">
                                                        <div className="illust-count-overlay"></div>
                                                        <div className="illust-count-content">
                                                            <img src="/images/icon/ic_photos.svg" /> {item.meta_pages.length}
                                                        </div>
                                                    </div> : <span />
                                            }
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                }
            }
        };
        if (this.props.userData.userIllustStatus.isFetching) {
            return <div></div>;
        } else {
            return (
                <ReactCSSTransitionGroup transitionName="anim" transitionAppear={true}
                    transitionAppearTimeout={1000} transitionLeaveTimeout={1500} transitionEnterTimeout={1500}>
                    <Grid {...gridData} />
                </ReactCSSTransitionGroup>
            );
        }
    };
    initUserMangaGrid = () => {
        let gridData = {
            lazyloading: true,
            data: this.props.userData.userMangaStatus.dataFetched ? this.props.userData.dataManga : [],
            columns: {
                columnsCount: 3,
                responsive: {
                    lg: 3,
                    md: 3,
                    sm: 3,
                    xs: 1,
                },
                content: {
                    format: (item) => {
                        return (
                            <div className="portlet light hvr-float">
                                <div className="portlet-body">
                                    <div className="illust-container">
                                        <a href={"#/ranking/detail/" + item.id}>
                                            <img className="img-responsive" src={item.image_urls.px_480mw} />
                                            {
                                                item.meta_pages.length > 0 ?
                                                    <div className="illust-count">
                                                        <div className="illust-count-overlay"></div>
                                                        <div className="illust-count-content">
                                                            <img src="/images/icon/ic_photos.svg" /> {item.meta_pages.length}
                                                        </div>
                                                    </div> : <span />
                                            }
                                        </a>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                }
            }
        };
        if (this.props.userData.userMangaStatus.isFetching) {
            return <div></div>;
        } else {
            return (
                <ReactCSSTransitionGroup transitionName="anim" transitionAppear={true}
                    transitionAppearTimeout={1000} transitionLeaveTimeout={1500} transitionEnterTimeout={1500}>
                    <Grid {...gridData} />
                </ReactCSSTransitionGroup>
            );
        }
    };
    render() {
        return (
            <div className="container-fluid profile">
                <div className="row">
                    <div className="col-md-12">
                        {this.renderUserProfile()}
                        <div className="profile-content">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="portlet light ">
                                        <div className="portlet-title tabbable-line">
                                            <div className="caption caption-md">
                                                <span className="caption-subject font-blue-madison bold uppercase">
                                                    れこ太's Works
                                                </span>
                                            </div>
                                            <ul className="nav nav-tabs">
                                                <li className="active">
                                                    <a href="#illust_tab" data-toggle="tab" aria-expanded="false"> Illustrations </a>
                                                </li>
                                                <li>
                                                    <a href="#manga_tab" data-toggle="tab" aria-expanded="true"> Manga </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="portlet-body">
                                            <div className="tab-content">
                                                <div id="illust_tab" className="tab-pane grid-layout active">
                                                    {this.initUserIllustGrid()}
                                                </div>
                                                <div id="manga_tab" className="tab-pane grid-layout">
                                                    {this.initUserMangaGrid()}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        userData: state.userData
    };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchingUserDetail: (userID) => dispatch(fetchingUserDetail(userID)),
        fetchUserWorks: (type, userID) => dispatch(fetchUserWorks(type, userID))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);