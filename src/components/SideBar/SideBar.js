import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class SideBar extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="page-sidebar-wrapper">
        <div className="page-sidebar navbar-collapse collapse">
          <ul className="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true"
              data-slide-speed="200">
            <li className="nav-item">
              <div className="fb-page" data-href="https://www.facebook.com/appanimeillust/" data-small-header="false" data-adapt-container-width="true"
                   data-hide-cover="false" data-show-facepile="false">
                <blockquote cite="https://www.facebook.com/appanimeillust/" className="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/appanimeillust/">Anime Illust</a></blockquote>
              </div>
            </li>

            <li className="nav-item active">
              <Link to="/ranking" className="nav-link nav-toggle">
                 <i className="fa fa-star"></i>
                <span className="title">Ranking</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/feed" className="nav-link nav-toggle">
                <i className="fa fa-photo"></i>
                <span className="title">Feed</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/gif" className="nav-link nav-toggle">
                <i className="fa fa-photo"></i>
                <span className="title">Gif</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/about" className="nav-link nav-toggle">
                <i className="fa fa-info-circle"></i>
                <span className="title">About</span>
              </Link>
            </li>
            <li className="heading">
              <h3 className="uppercase">Follow us on social media</h3>
            </li>
            <li className="nav-item">
              <a href="javascript:;" className="nav-link nav-toggle">
                <i className="fa fa-facebook-square"></i>
                <span className="title">Facebook</span>
              </a>
            </li>
            <li className="nav-item">
              <a href="javascript:;" className="nav-link nav-toggle">
                <i className="fa fa-instagram"></i>
                <span className="title">Instagram</span>
              </a>
            </li>
            <li className="nav-item">
              <a href="javascript:;" className="nav-link nav-toggle">
                <i className="fa fa-google-plus-square"></i>
                <span className="title">Google Plus</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default SideBar;
