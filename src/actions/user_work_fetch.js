import {
    FETCHING_USER_ILLUSTS, FETCHING_USER_ILLUSTS_SUCCESS, FETCHING_USER_ILLUSTS_FAILURE,
    FETCHING_USER_DETAIL, FETCHING_USER_DETAIL_SUCCESS, FETCHING_USER_DETAIL_FAILURE,
    FETCHING_USER_MANGA, FETCHING_USER_MANGA_SUCCESS, FETCHING_USER_MANGA_FAILURE
} from './constants';
import api from '../api/api';
import { formatRankingDataImageURL } from '../helper/image_helper';
import CookieHandler from '../helper/cookie_helper';

export function getUserIllust() {
    return {
        type: FETCHING_USER_ILLUSTS
    };
}

export function getUserIllustSuccess(data) {
    return {
        type: FETCHING_USER_ILLUSTS_SUCCESS,
        data
    };
}

export function getUserIllustFailure() {
    return {
        type: FETCHING_USER_ILLUSTS_FAILURE
    };
}

export function getUserManga() {
    return {
        type: FETCHING_USER_MANGA
    };
}

export function getUserMangaSuccess(data) {
    return {
        type: FETCHING_USER_MANGA_SUCCESS,
        data
    };
}

export function getUserMangaFailure() {
    return {
        type: FETCHING_USER_MANGA_FAILURE
    };
}

export function getUserDetail() {
    return {
        type: FETCHING_USER_DETAIL
    };
}

export function getUserDetailSuccess(detail) {
    return {
        type: FETCHING_USER_DETAIL_SUCCESS,
        detail
    };
}

export function getUserDetailFailure() {
    return {
        type: FETCHING_USER_DETAIL_FAILURE
    };
}

export function fetchingUserDetail(userID) {
    return async (dispatch) => {
        dispatch(getUserDetail());
        try {
            let tokenCookiesValue = await CookieHandler().getCookiesValue('tokenCookies');
            if (tokenCookiesValue != null) {
                let result = await api.getUserDetail(userID, tokenCookiesValue);
                dispatch(getUserDetailSuccess(result.data));
            }
        } catch (error) {
            dispatch(getUserDetailFailure());
        }
    };
}

export function fetchUserWorks(type, userID) {
    return async (dispatch) => {
        if (type == 'illust') {
            dispatch(getUserIllust());
        } else if (type == 'manga') {
            dispatch(getUserManga());
        }
        
        try {
            let tokenCookiesValue = await CookieHandler().getCookiesValue('tokenCookies');
            if (tokenCookiesValue != null) {
                let result = await api.getUserWork(type, userID, tokenCookiesValue);
                formatRankingDataImageURL(result.data.illusts);
                if (type == 'illust') {
                    dispatch(getUserIllustSuccess(result));
                } else if (type == 'manga') {
                    dispatch(getUserMangaSuccess(result));
                }
            }
        } catch (error) {
            if (type == 'illust') {
                dispatch(getUserIllustFailure());
            } else if (type == 'manga') {
                dispatch(getUserMangaFailure());
            }
        }
    };
}